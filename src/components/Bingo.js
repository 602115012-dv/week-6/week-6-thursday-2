import React from 'react';
import StartButton from './StartButton';
import Header from './Header';
import Menu from './Menu';
import Board from './Board';

class Bingo extends React.Component {
  state = {
    boardSize: null,
    maxBoardValue: 9,
    minBoardValue: 0,
    isStarted: false,
    selectedNumbers: [],
    generatedNumbers: [],
    win: false
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-4">
            {this.state.isStarted ? this.renderGame() : this.renderFrontPage()}
          </div>
        </div>
      </div>
    );
  }

  toggle = () => {
    this.setState({
      isStarted: !this.state.isStarted
    });
  }

  renderFrontPage = () => {
    return (
      <div style={{textAlign: "center"}}>
        <div className="form-group">
          <input type="number" className="form-control" 
          id="boardSize" placeholder="Board size"></input>
          <input type="number" className="form-control" 
          id="minValue" placeholder="Minimun value"></input>
          <input type="number" className="form-control" 
          id="maxValue" placeholder="Maximum value"></input>
        </div>
        <StartButton 
          setBoardSize={this.setBoardSize}
        />
      </div>
    );
  }

  setBoardSize = () => {
    let boardSize = parseFloat(document.getElementById("boardSize").value);
    let maxValue = parseFloat(document.getElementById("maxValue").value);
    let minValue = parseFloat(document.getElementById("minValue").value);

    if (!this.isPositiveInteger(boardSize)) {
      alert("The board size must be a positive integer!!!");
    } else if (!this.isPositiveInteger(minValue) && minValue !== 0) {
      alert("The minimum value must be a positive integer!!!");
    } else if (!this.isPositiveInteger(maxValue) && maxValue !== 0) {
      alert("The maximum value must be a positive integer!!!");
    } else if (maxValue < minValue) {
      alert("The maximum value must be larger than the minimum value!!!");
    } else {
      this.generateNumbers(boardSize, minValue, maxValue)
      this.setState({
        maxBoardValue: maxValue,
        minBoardValue: minValue,
        boardSize: boardSize
      });
      this.forceUpdate();
      this.toggle();
    }

    // if (this.isPositiveInteger(boardSize)) {
      
    // } else {
    //   alert("The board size must be a positive integer!!!");
    // }
  }

  renderGame = () => {
    return (
      <div className="row">
        <Header />
        <div className="w-100"></div>
        <Menu 
          toggle={this.toggle}
          maxBoardValue={this.state.maxBoardValue} 
          minBoardValue={this.state.minBoardValue}
          addSelectedNumber={this.addSelectedNumber}
          resetSelectedNumber={this.resetSelectedNumber}
          generatedNumbers={this.state.generatedNumbers}
          win={this.state.win}
          isWin={this.isWin}
          resetGameStatus={this.resetGameStatus}
        />
        <div className="w-100"></div>
        <div className="col" hidden={!this.state.win} 
        style={{textAlign: "center"}}>
          <h2>You Win!!!</h2>
        </div>
        <div className="w-100"></div>
        <Board 
          boardSize={this.state.boardSize} 
          maxBoardValue={this.state.maxBoardValue} 
          minBoardValue={this.state.minBoardValue}
          selectedNumbers={this.state.selectedNumbers}
          isMatched={this.isMatched}
          generatedNumbers={this.state.generatedNumbers}
        />
      </div>
    );
  }

  addSelectedNumber = (selectedNumber) => {
    let selectedNumbers = this.state.selectedNumbers;
    if (this.isDuplicate(selectedNumber)) {
      return;
    }
    selectedNumbers.push(selectedNumber);
    this.setState({
      selectedNumbers: selectedNumbers
    });
  }

  resetSelectedNumber = () => {
    this.setState({
      selectedNumbers: []
    });
  }

  resetGameStatus = () => {
    this.setState({
      win: false
    });
  }

  isDuplicate = (num) => {
    for (let i = 0; i < this.state.selectedNumbers.length; i++) {    
      if (num === Number(this.state.selectedNumbers[i])) {
        return true;
      }
    }
    return false;
  }
  
  isWin = () => {
    if (this.state.generatedNumbers.length === 0) {
      return;
    }

    let matchedNumberCounter = 0;

    //Check horizontally
    for (let i = 0; i < this.state.boardSize; i++) {
      matchedNumberCounter = 0;
      for (let j = 0; j < this.state.boardSize; j++) {
        if (this.isMatched(this.state.generatedNumbers[i][j])) {
          matchedNumberCounter++;
        }
      }

      if (matchedNumberCounter === this.state.boardSize) {
        this.setState({
          win: true
        });
      }
    }

    //Check vertically
    for (let i = 0; i < this.state.boardSize; i++) {
      matchedNumberCounter = 0;
      for (let j = 0; j < this.state.boardSize; j++) {
        if (this.isMatched(this.state.generatedNumbers[j][i])) {
          matchedNumberCounter++;
        }
      }

      if (matchedNumberCounter === this.state.boardSize) {
        this.setState({
          win: true
        });
      }
    }

    //Check diagonally
    //Check right-skewed
    matchedNumberCounter = 0;
    for (let i = 0; i < this.state.boardSize; i++) {
      if (this.isMatched(this.state.generatedNumbers[i][i])) {
        matchedNumberCounter++;
      }
    }

    if (matchedNumberCounter === this.state.boardSize) {
      this.setState({
        win: true
      });
    }

    //Check left-skewed
    matchedNumberCounter = 0;
    for (let i = 0; i < this.state.boardSize; i++) {
      if (this.isMatched(this.state.generatedNumbers[i][this.state.boardSize - 1 - i])) {
        matchedNumberCounter++;
      }
    }

    if (matchedNumberCounter === this.state.boardSize) {
      this.setState({
        win: true
      });
    }

    return false;
  }

  isMatched = (num) => {
    for (let i = 0; i < this.state.selectedNumbers.length; i++) {
      if (num === this.state.selectedNumbers[i]) {
        return true;
      }
    }
    return false;
  }

  generateNumbers = (boardSize, min, max) => {
    let generatedNumbers = [];
    // let max = this.state.maxBoardValue;
    // let min = this.state.minBoardValue;

    for (let i = 0; i < boardSize; i++) {
      let numberRow = [];
      for (let j = 0; j < boardSize; j++) {
        numberRow.push(Math.floor(Math.random() * (max - min + 1) + min));
      }
      generatedNumbers.push(numberRow);
    }

    this.setState({
      generatedNumbers: generatedNumbers
    });
  }

  isPositiveInteger = (num) => {
    if (Number.isInteger(num) && num > 0 && Number.isFinite(num)) {
      return true;
    }
    return false;
  }
}

export default Bingo;