import React from 'react';

const StartButton = (props) => {
  const startGame = () => {
    props.setBoardSize();
  }

  return (
    <div className="row">
      <div className="col" style={{textAlign: "center"}}>
        <button className="btn btn-primary btn-lg" onClick={startGame}>Start Game</button>
      </div>
    </div>
  );
}

export default StartButton;