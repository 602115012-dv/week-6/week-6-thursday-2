import React from 'react';

class Board extends React.Component {
  render() {
    return (
      <div className="col" style={{marginTop: "10px", 
      display: "flex", justifyContent: "center"}}>
        <table className="table table-bordered" 
        style={{textAlign: "center", fontSize: "25px"}}>
          <tbody id="board">
            {this.updateBoard()}
          </tbody>
        </table>
      </div>
    );
  }

  updateBoard = () => {
    let board = [];
    let key = 1;
    for (let i = 0; i < this.props.boardSize; i++) {
      board.push(
        <tr key={i}>
          {this.getRowData(i, key)}
        </tr>
      );
      key += this.props.boardSize;
    }
    return board;
  }

  getRowData = (i, key) => {
    if (this.props.generatedNumbers.length === 0) {
      return;
    }
    let rowData = [];
    for (let j = 0; j < this.props.boardSize; j++) {
      if (this.props.isMatched(this.props.generatedNumbers[i][j])) {
        rowData.push(
          <td key={key + j} style={{backgroundColor: "red"}}>{this.props.generatedNumbers[i][j]}</td>
        );
      } else {
        rowData.push(
          <td key={key + j}>{this.props.generatedNumbers[i][j]}</td>
        );
      }
    }
    return rowData;
  }
}

export default Board;