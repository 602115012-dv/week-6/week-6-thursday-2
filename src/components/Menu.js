import React from 'react';

const Menu = (props) => {
  const exit = () => {
    props.toggle();
    props.resetSelectedNumber();
    props.resetGameStatus();
  }

  const randomNumber = () => {
    if (!props.win) {
      let max = props.maxBoardValue;
      let min = props.minBoardValue;
      let randomNumber = Math.floor(Math.random() * (max - min + 1) + min);
      props.addSelectedNumber(randomNumber);
      alert(randomNumber);
      props.isWin();
    } else {
      alert("You already win!!!");
    }
  }

  return (
    <div className="col" style={{textAlign: "center"}}>
      <button type="button" className="btn btn-primary" onClick={randomNumber}>Random</button>
      <button type="button" className="btn btn-danger" onClick={exit}>Exit</button>
    </div>
  );
}

export default Menu;