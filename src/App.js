import React from 'react';
import './App.css';
import Bingo from './components/Bingo';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <Bingo />
    </div>
  );
}

export default App;
